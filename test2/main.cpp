#include <iostream>
#include "header.h"

using namespace std;

int main ()
{
    API api;
    cout << "The Answer to the Ultimate Question of Life, The Universe, and Everything is " << api.method() << endl;
    return 0;
}
