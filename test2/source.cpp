#include "header.h"

using namespace std;

API::API() {
    Py_Initialize();
    // Add pwd to the python search path
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("import os");
    PyRun_SimpleString("sys.path.append(os.getcwd())");

    // Import python module
    PyObject * pModule = PyImport_ImportModule("pytest");
    if (pModule == NULL) error("Can't import the python module");

    // Fetch python API class
    PyObject * pClass = PyObject_GetAttrString(pModule, "myclass");
    if (pClass == NULL) error("Can't get python class");

    // Create an instance of python class
    pInst = PyEval_CallObject(pClass, NULL);
    if (pInst == NULL) error("Can't create an instance of the python class");
    Py_DECREF(pClass);
    Py_DECREF(pModule);
}

API::~API() {
    Py_DECREF(pInst);
    Py_Finalize();
}

int API::method() {
    PyObject * pMethod = PyObject_GetAttrString(pInst, "answer_to_the_utimate_question");
    if (pMethod == NULL) error("Can't get method from python!");
    PyObject * pValue = PyObject_CallObject(pMethod, NULL);
    int result = PyLong_AsLong(pValue);
    Py_DECREF(pValue);
    Py_DECREF(pMethod);
    return result;
}
