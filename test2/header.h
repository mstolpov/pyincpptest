#define PY_SSIZE_T_CLEAN
#include <Python.h>

#define error(msg) do { printf("%s\n", msg); exit(1); } while (1)

#include <iostream>
using namespace std;

class API {
public:
    API();
    ~API();
    int method();
private:
    PyObject * pInst;
};
