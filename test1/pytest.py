def standalone_fun(x):
    return x * 2

class myclass:
    def __init__(self, arg=3):
        self.arg = arg

    def method_no_arg(self):
        return self.arg

    def method_with_arg(self, x):
        return x * self.arg

