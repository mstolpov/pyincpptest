#define PY_SSIZE_T_CLEAN
#include <Python.h>

#define error(msg) do { printf("%s\n", msg); exit(1); } while (1)

#include <iostream>

// Some docs:
// https://docs.python.org/3/extending/embedding.html
// https://docs.python.org/2.7/c-api/import.html
// https://root-forum.cern.ch/t/convert-root-class-to-pyobject/22007/3
// https://gitlab.cern.ch/andrii/pevspace-ml-tracking/-/tree/master/cpp/cpptest

using namespace std;

class api {
public:
    api(int i) {
        Py_Initialize();
        // Add pwd to the python search path
        PyRun_SimpleString("import sys");
        PyRun_SimpleString("import os");
        PyRun_SimpleString("sys.path.append(os.getcwd())");

        // Import python module
        PyObject * pModule = PyImport_ImportModule("pytest");
        if (pModule == NULL) {
            error("Can't import the python module");
        }

        // Fetch python API class
        PyObject * pClass = PyObject_GetAttrString(pModule, "myclass");
        if (pClass == NULL) error("Can't get python class");

        // Create an instance of python class
        PyObject * pArgs = Py_BuildValue("(i)", i);
        pInst = PyEval_CallObject(pClass, pArgs);
        if (pInst == NULL) error("Can't create an instance of the python class");
        Py_DECREF(pArgs);
        Py_DECREF(pClass);
        Py_DECREF(pModule);
    }

    ~api(){
        Py_DECREF(pInst);
        Py_Finalize();
    }

    int method_no_arg(){
        PyObject * pMethod = PyObject_GetAttrString(pInst, "method_no_arg");
        if (pMethod == NULL) error("Can't get method from python!");
        PyObject * pValue = PyObject_CallObject(pMethod, NULL);
        int result = PyLong_AsLong(pValue);
        Py_DECREF(pValue);
        Py_DECREF(pMethod);
        return result;
    }
private:
    PyObject * pInst;
};

int main ()
{
    // #################################################################
    // # Test of C++ API class functionality
    // #################################################################
    api a(2);
    cout << "Results = " << a.method_no_arg() << endl;

    // #################################################################
    // # More general tests
    // #################################################################

    PyObject *pModule, *pFunc, *pClass, *pInst, *pMethod_no_arg, *pMethod_with_arg, *pValue, *pArgs;
    Py_Initialize();

    // Add pwd to the python search path
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('/atlas/users/stolpovs/py_cpp_test/')");

    // Import python module
    pModule = PyImport_ImportModule("pytest");
    if (pModule == NULL) {
        error("Can't import the python module");
    }

    // #################################################################
    // # Working with python functions
    // #################################################################

    // Get standalone function from the module
    pFunc = PyObject_GetAttrString(pModule, "standalone_fun");
    if (pFunc == NULL) {
        error("Can't get the python function from the module");
    }

    // Using a standalong function:
    pArgs = PyTuple_New(1);
    pValue = PyLong_FromLong(2);
    PyTuple_SetItem(pArgs, 0, pValue);
    pValue = PyObject_CallObject(pFunc , pArgs);
    cout << "Got an output from the python standalone function (must be 4) : " << PyLong_AsLong(pValue) << endl;

    Py_DECREF(pArgs);
    Py_DECREF(pValue);
    Py_DECREF(pFunc);

    // #################################################################
    // # Working with python classes
    // #################################################################

    // Fetch python API class
    pClass = PyObject_GetAttrString(pModule, "myclass");
    if (pClass == NULL) error("Can't get python class");

    // Create an instance of python class
    pArgs = Py_BuildValue("(i)", 2);
    pInst = PyEval_CallObject(pClass, pArgs);
    if (pInst == NULL) error("Can't create an instance of the python class");
    Py_DECREF(pArgs);

    // Get functions from python code
    pMethod_no_arg = PyObject_GetAttrString(pInst, "method_no_arg");
    pMethod_with_arg = PyObject_GetAttrString(pInst, "method_with_arg");
    if (pMethod_no_arg == NULL || pMethod_with_arg == NULL) error("Can't get class methods from python");

    // Call method that has no arguments
    pValue = PyObject_CallObject(pMethod_no_arg, NULL);
    cout << "Got an output from the python class method without arguments (must be 2) : " << PyLong_AsLong(pValue) << endl;
    Py_DECREF(pValue);
    Py_DECREF(pMethod_no_arg);

    // Call method with arguments
    pArgs = Py_BuildValue("(i)", 3);
    pValue = PyObject_CallObject(pMethod_with_arg, pArgs);
    cout << "Got an output from the python class method with arguments (must be 6) : " << PyLong_AsLong(pValue) << endl;
    Py_DECREF(pArgs);
    Py_DECREF(pValue);
    Py_DECREF(pMethod_with_arg);
    Py_DECREF(pInst);

    // #################################################################
    // # Working with python classes
    // #################################################################

    pInst = PyEval_CallObject(pClass, NULL);
    if (pInst == NULL) error("Can't create an instance of the python class");
    
    pMethod_no_arg = PyObject_GetAttrString(pInst, "method_no_arg");
    if (pMethod_no_arg == NULL) error("Can't get class methods from python");
    pValue = PyObject_CallObject(pMethod_no_arg, NULL);
    cout << "Got an output from the python class method without arguments (must be 3) : " << PyLong_AsLong(pValue) << endl;
    Py_DECREF(pValue);
    Py_DECREF(pMethod_no_arg);
    Py_DECREF(pInst);
    Py_DECREF(pClass);
    Py_DECREF(pModule);

    Py_Finalize();
  
    return 0;
}
